from django.shortcuts import render
from promotion.models import Promotion
from product.models import Product, Category
from information.models import ContactMessage, ContactForm
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.core.mail import send_mail, BadHeaderError


# Create your views here.

def index(request):
	# return HttpResponse('Hello im on front page !')

	carousel = Promotion.objects.all()
	product_latest = Product.objects.all().order_by('-id')[:4]
	product_category_interior = Product.objects.filter(category__parent__slug='interior')[:4]
	product_category_eksterior = Product.objects.filter(category__parent__slug='eksterior')[:4]


	context = { 
		'carousel' : carousel,
		'product_latest' : product_latest,
		'product_cat_in' : product_category_interior,
		'product_cat_ek' : product_category_eksterior
	}

	return render(request, 'index.html', context)


def showrooms(request):
	return render(request, 'showroom.html')

def pay_shipping(request):
	return render(request, 'pembayaran-dan-pengiriman.html')

def contact(request):
	if request.method == 'POST': # check post
		form = ContactForm(request.POST)
		if form.is_valid():
			data = ContactMessage() #create relation with model
			data.name = form.cleaned_data['name'] # get form input data
			data.email = form.cleaned_data['email']
			data.subject = form.cleaned_data['subject']
			data.message = form.cleaned_data['message']
			data.ip = request.META.get('REMOTE_ADDR')
			data.save()  #save data to table
			messages.success(request,"Your message has ben sent. Thank you for your message.")
			try:
				send_mail(data.subject, data.message, data.email, ['denpri26@gmail.com'])
			except BadHeaderError:
				return HttpResponse('Invalid header found.')
			return HttpResponseRedirect('/contact')

	form = ContactForm

	context = {
		'form' : form
	}

	return render(request, 'contact.html', context)


def about(request):
	return render(request, 'about.html')