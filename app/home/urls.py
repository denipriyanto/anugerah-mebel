from django.urls import path
from . import views

app_name = 'home'
urlpatterns = [
	path('', views.index, name='index'),
	path('showrooms/', views.showrooms, name='showrooms'),
	path('pembayaran-dan-pengiriman/', views.pay_shipping, name='pay_shipping'),
	path('contact/', views.contact, name='contact'),
	path('about/', views.about, name='about'),
]