from django.urls import path
from . import views

app_name = 'katalog'
urlpatterns = [
	path('', views.katalog, name='katalog'),
	path('<slug:slug>/', views.katalog_slug, name='katalog_slug'),
]