from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.core.paginator import Paginator
from product.models import Product, Category

# Create your views here.

def katalog(request):
    """
    Katalog Page design
    """

    category = Category.objects.all()
    product_list = Product.objects.all()
    product_count = Product.objects.all().count()
    paginator = Paginator(product_list, 12)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)


    context = {
        'categories' : category,
        'page_obj' : page_obj,
        'product_count' : product_count
    }
    return render(request, 'katalog.html', context)


def katalog_slug(request, slug):
    # products = Product.objects.get(category_id=id)

      # First get the category object.
    category = get_object_or_404(Category, slug=slug)
    category_all = Category.objects.all()
    
    # then filter the products on this category

    products =Product.objects.filter(category=category)
    product_count = products.count()

    context = {
        'categories' : category_all,
        'page_obj' : products,
        'product_count' : product_count
    }

    return render(request, 'katalog.html' , context)
    
