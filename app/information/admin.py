from django.contrib import admin
from .models import ContactMessage

# Register your models here.

class ContactMessageAdmin(admin.ModelAdmin):
    list_display = ['name','subject', 'update_at','status']
    readonly_fields =('name','subject','email','message','ip')
    list_filter = ['status']

admin.site.register(ContactMessage, ContactMessageAdmin)