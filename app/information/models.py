from django.db import models
from django.forms import ModelForm, TextInput, Textarea
from django.utils.safestring import mark_safe
from django.http import request


# Create your models here.

class ContactMessage(models.Model):
    STATUS = (
        ('New', 'New'),
        ('Read', 'Read'),
        ('Closed', 'Closed'),
    )

    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    subject = models.CharField(max_length=50)
    message = models.TextField(max_length=255)
    status = models.CharField(max_length=10, choices=STATUS, default='New')
    ip = models.CharField(blank=True, max_length=20)
    note = models.CharField(blank=True, max_length=255)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class ContactForm(ModelForm):
    class Meta:
        model = ContactMessage
        fields = ['name', 'email', 'subject', 'message']
        widgets = {
            'name'   : TextInput(attrs={'class': 'form-control input','placeholder':'Name & Surname'}),
            'subject' : TextInput(attrs={'class': 'form-control input','placeholder':'Subject'}),
            'email'   : TextInput(attrs={'class': 'form-control input','placeholder':'Email Address'}),
            'message' : Textarea(attrs={'class': 'form-control input','placeholder':'Your Message','rows':'5'}),
        }

