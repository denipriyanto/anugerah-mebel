from django.shortcuts import render
from django.http import HttpResponse
from django.core.paginator import Paginator
from .models import Product, Category, Images

# Create your views here.

def product_detail(request, id, slug):
    """
    docstring
    """
    product_detail = Product.objects.get(slug=slug)
    images = Images.objects.filter(product_id=id).order_by('id')
    product_random = Product.objects.all().order_by('?')[:4]
    

    print(images)

    context = {
        'product' : product_detail,
        'images' : images,
        'product_random' : product_random
    }
    

    return render(request, 'product_detail.html', context)
