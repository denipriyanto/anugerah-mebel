from django.db import models
from django.forms import ModelForm, TextInput, Textarea
from django.http import request
from django.utils.safestring import mark_safe
from django.urls import reverse
from product.models import Product, Variants

# Create your models here.

class Order(models.Model):
    STATUS = (
        ('New', 'New'),
        ('Accepted', 'Accepted'),
        ('Preaparing', 'Preaparing'),
        ('OnShipping', 'OnShipping'),
        ('Completed', 'Completed'),
        ('Canceled', 'Canceled'),
    )

    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    variants = models.ForeignKey(Variants, on_delete=models.SET_NULL,blank=True, null=True)
    price = models.FloatField()
    quantity = models.IntegerField()
    status = models.CharField(max_length=10,choices=STATUS,default='New')
    ip = models.CharField(blank=True, max_length=20)
    adminnote = models.CharField(blank=True, max_length=100)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.product.title

    def get_absolute_url(self):
        return reverse("order_detail", kwargs={"pk": self.pk})
