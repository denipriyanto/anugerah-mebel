import admin_thumbnails
from django.contrib import admin
from .models import Order, Variants


# Register your models here.


class OrderAdmin(admin.ModelAdmin):
    list_display = ['product', 'variants', 'quantity', 'status', 'adminnote']
    readonly_fields = ('product', 'variants', 'quantity', 'price')
    list_filter = ['status']


admin.site.register(Order, OrderAdmin)