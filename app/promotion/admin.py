import admin_thumbnails
from django.contrib import admin
from .models import Promotion

# Register your models here.

class PromotionAdmin(admin.ModelAdmin):
    list_display = ['title', 'url', 'status', 'image_tag']

admin.site.register(Promotion, PromotionAdmin)