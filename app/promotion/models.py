from django.db import models
from django.urls import reverse
from django.db.models import Avg, Count
from django.utils.safestring import mark_safe


# Create your models here.

class Promotion(models.Model):
    
    STATUS = (
        ('True', 'True'),
        ('False', 'False'),
    )

    title = models.CharField(max_length=50)
    sub_title = models.TextField(max_length=255)
    image = models.ImageField(upload_to="images/", null=False)
    url = models.CharField(max_length=150)
    triger_button = models.CharField(max_length=50)
    status = models.CharField(max_length=10, choices=STATUS)

    def __str__(self):
        return self.title

    def image_tag(self):
        if self.image.url is not None:
            return mark_safe('<img src="{}" height="50"/>'.format(self.image.url))
        else:
            return ""
    

